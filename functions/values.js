function values(obj) {
  const result = [];
  for (let key in obj) {
    if (obj.hasOwnProperty(key) && typeof obj[key] !== "function") {
      result.push(obj[key]);
    }
  }
  return result;
}
module.exports = values;