function invert(obj) {
  const invertedObject = {};
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      invertedObject[obj[key]] = key;
    }
  }
  return invertedObject;
}
module.exports = invert;