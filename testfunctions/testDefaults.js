const defaults = require("../functions/defaults.js");
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
const anotherObject = { job: "Web Developer", country: "India" };
console.log(defaults(testObject, anotherObject));